# Comandos do GIT 

## git init  
Inicia o repositório GIT no diretório atual.

## git add 
("git add *" Adiciona todos não ignorados)
Adiciona arquivos/diretórios ao índice GIT

## git rm --cached [nome_arquivo]
Remove arquivos/diretórios do índice GIT

## git commit
("git commit -m "Mensagem")
Adiciona um commit a branch atual

## git status
Mostra o status do commit atual

## git remote add [nome_origem] [link]
Adiciona uma origem remota

## git push [nome_origem] [brach]
Envia a branch para o repositório remoto

## git branch
Mostra as branches ativas e a atual

## git branch -d [nome_branch]
Deleta a branch

## git push [nome_origem] --delete [nome_branch]
Deleta a branch remota

## git checkout [branch]
Muda para a branch descrita

## git checkout -b [branch]
Cria uma branch

## git diff [branch_origem] [branch_compare]
Compara as diferenças entre as duas branches

## git merge [branch]
Merge entre a branch atual e a descrita

## git pull 
Puxa as alterações do repositório remoto